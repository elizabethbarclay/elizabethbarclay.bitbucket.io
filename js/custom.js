// menu;
window.onscroll = function () {
  scrollFunction();
};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    document.getElementById("navbar").style.top = "0";
  } else {
    document.getElementById("navbar").style.top = "-70px";
  }
}

//hamburger
$(".hamburger").click(function () {
  $(".header nav").toggleClass("open");
});

//slide toggle in hamburger
// $(".slide").click(function () {
//   var target = $(this).parent().children(".dropdown-content");
//   $(target).slideToggle();
// });

//slider
$(".mySlider").slick({
  arrows: false,
  autoplay: false,
  // autoplaySpeed: 3000,
  dots: true,
  infinite: false,
  speed: 300,
  slidesToShow: 5,
  slidesToScroll: 2,
  responsive: [
    {
      breakpoint: 1800,
      settings: {
        arrows: false,
        autoplay: false,
        // autoplaySpeed: 3000,
        slidesToShow: 4,
        slidesToScroll: 2,
        infinite: false,
        dots: true,
      },
    },
    {
      breakpoint: 1440,
      settings: {
        arrows: false,
        autoplay: false,
        // autoplaySpeed: 3000,
        slidesToShow: 3,
        slidesToScroll: 2,
        infinite: false,
        dots: true,
      },
    },
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: false,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
      },
    },
  ],
});

//subscribe popup
$(".subscribe-popup").click(function () {
  $(".popup").css("display", "flex");

  setTimeout(function () {
    $(".popup").addClass("open");
  }, 0);
});

$(".popup_mask, .popup_close").click(function () {
  $(".popup").removeClass("open");

  setTimeout(function () {
    $(".popup").css("display", "none");
  }, 401);
});
