// menu;
window.onscroll = function () {
  scrollFunction();
};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    document.getElementById("navbar").style.top = "0";
  } else {
    document.getElementById("navbar").style.top = "-70px";
  }
}

//hamburger
$(".hamburger").click(function () {
  $(".header nav").toggleClass("open");
});

//image detail popup
$(".text").click(function () {
  $(".popup").css("display", "flex");

  setTimeout(function () {
    $(".popup").addClass("open");
  }, 0);
});

$(".popup_mask, .popup_close").click(function () {
  $(".popup").removeClass("open");

  setTimeout(function () {
    $(".popup").css("display", "none");
  }, 401);
});

//subscribe popup
$(".subscribe-popup").click(function () {
  $(".pop").css("display", "flex");

  setTimeout(function () {
    $(".pop").addClass("open");
  }, 0);
});

$(".pop_mask, .pop_close").click(function () {
  $(".pop").removeClass("open");

  setTimeout(function () {
    $(".pop").css("display", "none");
  }, 401);
});
